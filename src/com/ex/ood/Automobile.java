package com.ex.ood;

import java.awt.Color;

public abstract class Automobile
{
	Color color;
       public Automobile(Color color)
	{
		this.color =color;
	   
	}

	public abstract int getCapacity();
	
	   //abstract이므로 비어놓음
}
