package com.ex.ood;

public class Ch7LightOffCommand implements Ch7Command {
	Ch7Light light;
	public Ch7LightOffCommand(Ch7Light light) {
		this.light=light;
	}
	@Override
	public void execute() {
		light.off();
	}
}
