package com.ex.ood;

import javax.swing.*;
import java.awt.*;

public class Ch8DrawingFrame extends JFrame{
	public Ch8DrawingFrame() {
	super("Drawing Application");
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	
	JComponent drawingCanvas = createDrawingCanvas();
	add(drawingCanvas, BorderLayout.CENTER);
	
	JToolBar toolbar = createToolbar();
	add(toolbar, BorderLayout.NORTH);
	}

	private JComponent createDrawingCanvas() {
		JComponent drawingCanvas = new JPanel();
		drawingCanvas.setPreferredSize(new Dimension(400,300));
		drawingCanvas.setBackground(Color.white);
		drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
		return drawingCanvas;
	}

	private JToolBar createToolbar() {
	JToolBar toolbar = new JToolBar();
	JButton ellipseButton = new JButton("Ellipse");
	toolbar.add(ellipseButton);
	JButton squareButton = new JButton("Square");
	toolbar.add(squareButton);
	JButton rectButton = new JButton("Rect");
	toolbar.add(rectButton);
 	return toolbar;
	}
}
