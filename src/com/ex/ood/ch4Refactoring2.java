package com.ex.ood;

public class ch4Refactoring2 {
	public double getTotalBill() {
		double mealCharge = 3.8;
		double movieCharge = 2.6;
		return getRoomCharge() + mealCharge + movieCharge;
	}
	public double getRoomCharge() {
		return 50.4;
	}
}
