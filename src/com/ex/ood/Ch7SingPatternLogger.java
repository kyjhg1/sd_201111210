package com.ex.ood;

public class Ch7SingPatternLogger {
	public static int n=0;
	private static Ch7SingPatternLogger uniqueInstance;
	private Ch7SingPatternLogger(){
		n++;
	}
	public static Ch7SingPatternLogger getInstance() {
		if(uniqueInstance == null)
			uniqueInstance = new Ch7SingPatternLogger();
			return uniqueInstance;
	}
	public int readEntireLog() {
		return n;
	}
}
