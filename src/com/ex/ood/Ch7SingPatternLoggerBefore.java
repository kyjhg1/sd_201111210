package com.ex.ood;

public class Ch7SingPatternLoggerBefore {
	public static int n=0;
	public Ch7SingPatternLoggerBefore() {
		n+=1;
	}
	public void writeLine(String text){}
	public int readEntireLog(){
		return n;
	}
}
