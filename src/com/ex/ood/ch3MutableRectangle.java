package com.ex.ood;

import com.ex.ood.ch3Rectangle;

public class ch3MutableRectangle extends ch3Rectangle {
	
	public ch3MutableRectangle(int x, int y, int w, int h) {
		super(x, y, w, h);
	}

	public void setSize(int w1, int h1) {	
		w = w1;
		h = h1;
	}
}
