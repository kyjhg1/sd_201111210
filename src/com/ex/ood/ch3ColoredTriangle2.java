package com.ex.ood;

import java.awt.*;
import java.lang.*;
public class ch3ColoredTriangle2 extends ch3Triangle{
	private Color color;
	public ch3ColoredTriangle2(Color c, Point p1, Point p2, Point p3) {			
		super(p1, p2, p3);
		if(c==null) 
		c=Color.red;
		color=c;
	}
	public boolean equals(Object obj){
		if(obj instanceof ch3ColoredTriangle2) {
		ch3ColoredTriangle2 otherTriangle = (ch3ColoredTriangle2) obj;
		return super.equals(otherTriangle)&&this.color.equals(otherTriangle.color);
	}
	else if(obj instanceof ch3Triangle) {
		return super.equals(obj);
	}
	else
	return false;
}
		
}	
