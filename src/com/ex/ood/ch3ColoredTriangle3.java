package com.ex.ood;

import java.awt.*;
import java.lang.*;

public class ch3ColoredTriangle3 extends ch3Triangle2 {
	private Color color;
	public ch3ColoredTriangle3(Color c, Point p1, Point p2, Point p3) {
		super(p1, p2, p3);
		if(c==null)
		c=Color.red;
		color = c;
	}
	public boolean equals(Object obj) {
		if(obj==null)
		return false;
		if(obj.getClass()!=this.getClass())
		return false;
		if(!super.equals(obj) ) 
		return false;

		ch3ColoredTriangle3 otherTriangle = (ch3ColoredTriangle3)obj;
		return this.color.equals(otherTriangle.color);
	}
}
