package com.ex.ood;

public class ch4Refactoring3 {

	public double getTotalBill() {
		return getRoomCharge() + getMealCharge() + getMovieCharge();
	}
	public double getRoomCharge() {
		return 50.6;
	}
	public double getMealCharge() {
		return 20.5;
	}
	public double getMovieCharge() {
		return 17.6;
	}
}	
