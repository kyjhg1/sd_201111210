package com.ex.ood;

import java.awt.Color;
import com.ex.ood.Automobile;

public class Sedan extends Automobile
{
	public Sedan(Color color)
	{
		super(color);
	}
	public int getCapacity()
	{	
		return 5;
	}
}
