package com.ex.ood;

public class ch3Person {
	
	private String name;
	private String address;

	public ch3Person(String n, String ad) {
		name = n;
		address = ad;
	}

	public String getName(){
		return name;
	}

	public String getAddress() {
		return address;
	}
}
