package com.ex.ood.ch8.draw1;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import com.ex.ood.ch8.draw1.Ch8CanvasEditor;
import com.ex.ood.ch8.draw1.Ch8DrawingFrame;

public class Ch8ver1main 
{
   public static void main(String[] args)
    {
	Ch8DrawingFrame drawFrame = new Ch8DrawingFrame();
		drawFrame.pack();
		drawFrame.setVisible(true);
	}
} 
