package com.ex.ood.ch8.print;

public class NoQuarterState implements State
{
        GumballMachine gumball;
        public NoQuarterState(GumballMachine gumballMachine)
        {
                gumball=gumballMachine;
        }
        public void insertQuarter()
        {
                System.out.println("동전이 투입 되었습니다.");
		gumball.setState(gumball.getHasQuarterState());
        }
        public void ejectQuarter()
        {
                System.out.println("동전을 넣어주세요.");
        }
        public void turnCrank()
        {
                System.out.println("동전을 넣어주세요.");
        }
        public void dispense()
        {
		System.out.println("동전을 넣어주세요.");
	}
}
