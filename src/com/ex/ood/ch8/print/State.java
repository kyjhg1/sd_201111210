package com.ex.ood.ch8.print;

interface State
{
	public void insertQuarter();
	public void ejectQuarter();
	public void turnCrank();
	public void dispense();
}
