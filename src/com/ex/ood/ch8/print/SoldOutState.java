package com.ex.ood.ch8.print;
public class SoldOutState implements State
{
        GumballMachine gumball;
        public SoldOutState(GumballMachine gumballMachine)
        {
                gumball=gumballMachine;
        }
        public void insertQuarter()
        {
                System.out.println("매진되었습니다.");
        }
        public void ejectQuarter()
        {
                System.out.println("매진 되었습니다.");
        }
        public void turnCrank()
        {
                System.out.println("매진되었습니다..");
        }
        public void dispense()
	{
		System.out.println("매진되었습니다.");
	}
}
