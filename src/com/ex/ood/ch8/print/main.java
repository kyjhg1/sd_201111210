package com.ex.ood.ch8.print;

import com.ex.ood.ch8.print.GumballMachine;
import com.ex.ood.ch8.print.State;
import com.ex.ood.ch8.print.SoldState;
import com.ex.ood.ch8.print.SoldOutState;
import com.ex.ood.ch8.print.NoQuarterState;
import com.ex.ood.ch8.print.HasQuarterState;



public class main {

	public static void main(String[] args) {
	GumballMachine gum = new GumballMachine(5);

	System.out.println("ball count: " + gum.getCount());
	
	gum.insertQuarter();
	gum.turnCrank();

        System.out.println("ball count: " + gum.getCount());

	gum.insertQuarter();
	gum.ejectQuarter();
	gum.turnCrank();

        System.out.println("ball count: " + gum.getCount());

	gum.insertQuarter();
	gum.turnCrank();
	gum.insertQuarter();
	gum.turnCrank();
	gum.ejectQuarter();

        System.out.println("ball count: " + gum.getCount());

	gum.insertQuarter();
	gum.insertQuarter();
	gum.turnCrank();
	gum.insertQuarter();
	gum.turnCrank();
	gum.insertQuarter();
	gum.turnCrank();

        System.out.println("ball count: " + gum.getCount());

	}
}
