package com.ex.ood.ch8.print;
public class SoldState implements State
{
	GumballMachine gumball;
	public SoldState(GumballMachine gumballMachine)
	{
		gumball=gumballMachine;
	}
	public void insertQuarter()
	{
		System.out.println("잠시만 기다려 주세요.");
	}
	public void ejectQuarter()
	{
		System.out.println("이미 알맹이를 뽑으셨습니다.");
	}
	public void turnCrank()
	{
		System.out.println("손잡이는 한 번만 돌리세요.");
	}
	public void dispense()
	{
		gumball.releaseBall();
		if(gumball.getCount() >0)
		{
			gumball.setState(gumball.getNoQuarterState());
		}
		else
		{
			System.out.println("매진");
			gumball.setState(gumball.getSoldOutState());
		}
	}
}
