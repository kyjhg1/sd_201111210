package com.ex.ood.ch8.print;

public class HasQuarterState implements State
{
	GumballMachine gumball;
	public HasQuarterState(GumballMachine gumballMachine)
	{
		this.gumball=gumballMachine;
	}
	public void insertQuarter()
	{
		System.out.println("동전은 한개만 넣어주세요.");
	}
	public void ejectQuarter()
	{
		System.out.println("동전이 반환 됩니다.");
		gumball.setState(gumball.getNoQuarterState());
	}
	public void turnCrank()
	{
		System.out.println("손잡이를 돌리셨습니다.");
		gumball.setState(gumball.getSoldState());
	}
	public void dispense()
	{
		System.out.println("알맹이가 나갈 수가 없습니다.");
	}
}
