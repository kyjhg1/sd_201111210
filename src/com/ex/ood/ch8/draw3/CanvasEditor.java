package com.ex.ood.ch8.draw3;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import com.ex.ood.ch8.draw3.Figure;

public class CanvasEditor implements MouseListener
{
        private Figure currentFigure;
        public CanvasEditor(Figure figure)
        {
                this.currentFigure=figure;
        }
	public void setCurrentFigure(Figure figure)
	{
		currentFigure=figure;
	}
	public void mouseClicked(MouseEvent e)
        {
		Figure newFigure = (Figure) currentFigure.clone();
		newFigure.setCenter(e.getX(), e.getY());
		((DrawingCanvas) e.getSource()).addFigure(newFigure);
        }
        public void mouseEntered(MouseEvent e)
        {
        }
        public void mouseExited(MouseEvent e)
        {
        }
        public void mousePressed(MouseEvent e)
        {
        }
 	public void mouseReleased(MouseEvent e)
        {
        }
}
