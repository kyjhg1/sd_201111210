package com.ex.ood.ch8.draw3;

import java.awt.*;

public class Ellipse extends Figure
{
	public Ellipse(int x,int y,int w,int h)
	{
		super(x,y,w,h);
	}
	public void draw(Graphics g)
	{
		g.drawOval(getCenterX(),getCenterY(),getWidth(),getHeight());
	}
}
