package com.ex.ood.ch8.draw3;
import com.ex.ood.ch8.draw3.DrawingFrame;
import com.ex.ood.ch8.draw3.CanvasEditor;
import com.ex.ood.ch8.draw3.Drawing;
import com.ex.ood.ch8.draw3.DrawingCanvas;
import com.ex.ood.ch8.draw3.Ellipse;
import com.ex.ood.ch8.draw3.Figure;
import com.ex.ood.ch8.draw3.Rect;
import com.ex.ood.ch8.draw3.Square;

import javax.swing.*;
import java.util.*;
import java.awt.*;

public class ch8mainV3
{
        public static void main(String[] args)
        {
                DrawingFrame drawFrame =new DrawingFrame();
                drawFrame.pack();
                drawFrame.setVisible(true);
        }
}
