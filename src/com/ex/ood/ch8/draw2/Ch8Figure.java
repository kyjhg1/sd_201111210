package com.ex.ood.ch8.draw2;

import java.awt.*;

public abstract class Figure{
	private int centerX, centerY;
	private int width, height;
	public Figure(int centerX, int centerY, int w, int h){
		this.centerX = centerX;
		this.centerY = centerY;
		width = w;
		height = h;
	}
	public int getWidth(){
	return width;
	}
	public int getHeight() {
		return height;
	}

	public void setCenter(int centerX, int centerY) {
		this.centerX = centerX;
		this.centerY = centerY;
	}
	public int getCenterX(){
		return CenterX;
	}
	public int getCenterY(){
		return CenterY;
	}

	public abstract void draw();
}
