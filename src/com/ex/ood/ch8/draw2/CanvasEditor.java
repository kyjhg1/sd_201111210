package com.ex.ood.ch8.draw2;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class CanvasEditor implements MouseListener
{
        private Figure currentFigure;
        public CanvasEditor(Figure figure)
        {
                this.currentFigure=figure;
        }
	public void setCurrentFigure(Figure figure)
	{
		currentFigure=figure;
	}
	public void mouseClicked(MouseEvent e)
        {
                JPanel canvas=(JPanel)e.getSource();
		currentFigure.setCenter(e.getX(),e.getY());
		currentFigure.draw(canvas.getGraphics());
        }
        public void mouseEntered(MouseEvent e)
        {
        }
        public void mouseExited(MouseEvent e)
        {
        }
        public void mousePressed(MouseEvent e)
        {
        }
 	public void mouseReleased(MouseEvent e)
        {
        }
}
