package com.ex.ood.ch8.draw1;

import javax.swing.*;
import java.awt.*;
import com.ex.ood.ch8.draw1.Ch8CanvasEditor;

public class Ch8DrawingFrame extends JFrame{
	public Ch8DrawingFrame() {
		super("Drawing Application");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	
		JComponent drawingCanvas = createDrawingCanvas();
		add(drawingCanvas, BorderLayout.CENTER);
	
		JToolBar toolbar = createToolBar(drawingCanvas);
		add(toolbar, BorderLayout.NORTH);
	}

	private JComponent createDrawingCanvas() {
		JComponent drawingCanvas = new JPanel();
		drawingCanvas.setPreferredSize(new Dimension(400,300));
		drawingCanvas.setBackground(Color.white);
		drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
		return drawingCanvas;
	}

	private JToolBar createToolBar(JComponent canvas) { //마우스리스너사용하므로 canvas 받아옴.
	JToolBar toolbar = new JToolBar();
	JButton ellipseButton = new JButton("Ellipse");
	toolbar.add(ellipseButton);
	JButton squareButton = new JButton("Square");
	toolbar.add(squareButton);
	JButton rectButton = new JButton("Rect");
	toolbar.add(rectButton);
	
	//add the CnavasEditor listemer to the canvas and to the buttons
	//with the ellipseButon initially selected
	Ch8CanvasEditor canvasEditor = new Ch8CanvasEditor(ellipseButton);
	ellipseButton.addActionListener(canvasEditor);
	squareButton.addActionListener(canvasEditor);
	rectButton.addActionListener(canvasEditor);
	canvas.addMouseListener(canvasEditor);



 	return toolbar;
	}
}
