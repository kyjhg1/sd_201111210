package com.ex.ood.ch8.draw2;

import java.awt.*;

public abstract class Figure
{
	private int centerX;
	private int centerY;
	private int width;
	private int height;

	public Figure(int x,int y,int w,int h)
	{
		centerX=x;
		centerY=y;
		width=w;
		height=h;
	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
	public void setCenter(int x,int y)
	{
		centerX=x;
		centerY=y;
	}
	public int getCenterX()
	{
		return centerX;
	}
	public int getCenterY()
	{
		return centerY;
	}
	public abstract void draw(Graphics g);
}
