package com.ex.ood.ch8.draw4;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DrawingFrame extends JFrame 
{
   
    public DrawingFrame()     
    {
        super("Drawing Application");

        DrawingCanvas drawingCanvas = new DrawingCanvas();
        add(drawingCanvas, BorderLayout.CENTER);
        JToolBar toolbar = new JToolBar();
        add(toolbar, BorderLayout.NORTH);
        addToolsAndEditors(toolbar, drawingCanvas);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    private void addToolsAndEditors(JToolBar toolbar, DrawingCanvas canvas)     {
        final JButton selectionButton = new JButton("Select");
        toolbar.add(selectionButton);
        toolbar.addSeparator();
        final JButton ellipseButton = new JButton("Ellipse");
        toolbar.add(ellipseButton);
        final JButton squareButton = new JButton("Square");
        toolbar.add(squareButton);
        final JButton rectButton = new JButton("Rect");
        toolbar.add(rectButton);

        ellipseButton.setBorder(new LineBorder(Color.red, 1));
        ActionListener selectionListener = new ActionListener() {
            private JButton currentButton = ellipseButton;
            public void actionPerformed(ActionEvent ae)
	    {
                JButton newButton = (JButton) ae.getSource();
                if (newButton != currentButton) 
		{
                    Border selectedBorder = currentButton.getBorder();
                    currentButton.setBorder(newButton.getBorder());
                    newButton.setBorder(selectedBorder);
                    currentButton = newButton;
                }
            }
        };
        final CreationTool ellipseTool = new CreationTool(new Ellipse(0, 0, 60, 40));
        final CreationTool squareTool = new CreationTool(new Square(0, 0, 50));
        final CreationTool rectTool = new CreationTool(new Rect(0, 0, 60, 40));
        final SelectionTool selectTool = new SelectionTool();
        final CanvasEditor canvasEditor = new CanvasEditor(ellipseTool);

        selectionButton.addActionListener(selectionListener);
        selectionButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae)            
	    {
                canvasEditor.setCurrentTool(selectTool);
            }
        });
        ellipseButton.addActionListener(selectionListener);
        ellipseButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae)
	    {
                canvasEditor.setCurrentTool(ellipseTool);
            }
        });
        squareButton.addActionListener(selectionListener);
        squareButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) 
	    {
                canvasEditor.setCurrentTool(squareTool);
            }
        });
        rectButton.addActionListener(selectionListener);
        rectButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae)
	    {
                canvasEditor.setCurrentTool(rectTool);
            }
        });
        canvas.addMouseListener(canvasEditor);
        canvas.addMouseMotionListener(canvasEditor);
    }
}
