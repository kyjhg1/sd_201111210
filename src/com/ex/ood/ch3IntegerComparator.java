package com.ex.ood;
import com.ex.ood.ch3Comparator;

public class ch3IntegerComparator implements ch3Comparator {

	public int compare(Object o1, Object o2) {
		int s1 = (int)o1;
		int s2 = (int)o2;
		
		return s1-s2;
	}
}
