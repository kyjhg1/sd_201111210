package com.ex.ood;

import com.ex.ood.Automobile;
import java.awt.Color;

public class SportsCar  extends Automobile
{
	public SportsCar(Color color)
	{
		super(color);
	}
        public int getCapacity()
        {       
		return 2;
        }
}	
