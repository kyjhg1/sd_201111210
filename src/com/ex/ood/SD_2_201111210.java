package com.ex.ood;

import java.util.Observer;
import java.util.Observable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Iterator;
import java.awt.*;
import java.lang.*;
import com.ex.ood.EnhancedRectangle;
import com.ex.ood.Minivan;
import com.ex.ood.SportsCar;
import com.ex.ood.Automobile;
import com.ex.ood.Sedan;
import com.ex.ood.ch3MutableRectangle;
import com.ex.ood.ch3MutableSquare;
import com.ex.ood.ch3Rectangle;
import com.ex.ood.ch3Person;
import com.ex.ood.ch3Employee;
import com.ex.ood.ch3Student;
import com.ex.ood.ch3Sorter;
import com.ex.ood.ch3Comparator;
import com.ex.ood.ch3IntegerComparator;
import com.ex.ood.ch3StringComparator;
import com.ex.ood.ch3Triangle;
import com.ex.ood.ch3ColoredTriangle;
import com.ex.ood.ch3ColoredTriangle2;
import com.ex.ood.ch3Triangle2;
import com.ex.ood.ch4getRoomCharge;
import com.ex.ood.ch4Refactoring;
import com.ex.ood.ch4Refactoring2;
import com.ex.ood.ch4Refactoring3;
import com.ex.ood.Ch5FixedPointv1New;
//import com.ex.ood.Ch5FixedPointv1NewSub;
import com.ex.ood.Ch5FixedPointv2Inheritance;
import com.ex.ood.Ch5FixedPointv3Association;
import com.ex.ood.Ch5ObsPatternView;
import com.ex.ood.Ch5ObsPatternModel;
import com.ex.ood.Ch5ArrayListIterator;
import com.ex.ood.Ch5ArrayLinkedList;
import com.ex.ood.Ch7SingPatternLoggerBefore;
import com.ex.ood.Ch7SingPatternLogger;
import com.ex.ood.Ch7Duck;
import com.ex.ood.Ch7MallardDuck;
import com.ex.ood.Ch7Turkey;
import com.ex.ood.Ch7WildTurkey;
import com.ex.ood.Ch7TurkeyAdapter;
import com.ex.ood.Ch7Command;
import com.ex.ood.Ch7SimpleRemoteControl;
import com.ex.ood.Ch7Light;
import com.ex.ood.Ch7LightOnCommand;
import com.ex.ood.Ch7LightOffCommand;
import com.ex.ood.Ch7GarageDoor;
import com.ex.ood.Ch7GarageDoorOpenCommand;
import com.ex.ood.Ch7GarageDoorDownCommand;
import com.ex.ood.Ch7PizzaStore;
import com.ex.ood.Ch7Pizza;
import com.ex.ood.Ch7CheesePizza;
import com.ex.ood.Ch7SimplePizzaFactory;
import com.ex.ood.Ch8PushCounterPanel;
import javax.swing.JFrame;
import com.ex.ood.Ch8DrawingFrame;

public class SD_2_201111210
{
   public static void main(String[] args)
    { 
	//ch2main1();
	//ch2main2();
 	//ch2main3();
	//ch3main1();
	//ch3main2(); 
	//ch3main3();
	//ch4main1version1();
	//ch4main1version2();
	//ch4main1version3();
	//ch4main2();
	//ch5main1();
	//ch5main2();
	//ch5main3();
  	//ch7main1();
	//ch7main2();
	//ch7main3();
	//ch7main4();
	//ch8main1();
	//ch8main2();
	ch8main3();
 }
	public static void ch8main3() {
		Ch8DrawingFrame drawFrame = new Ch8DrawingFrame();
		drawFrame.pack();
		drawFrame.setVisible(true);
	}
	public static void ch8main2() {
		JFrame frame = new JFrame("push Counter");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Ch8PushCounterPanel panel = new Ch8PushCounterPanel();
 		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
	}
	public static void ch8main1() {
                Ch8SimpleDrawMain drawFrame = new Ch8SimpleDrawMain();
                drawFrame.setVisible(true);
        }
	public static void ch7main4(){
		Ch7SimplePizzaFactory fac = new Ch7SimplePizzaFactory();
		Ch7PizzaStore sto = new Ch7PizzaStore(fac);
		sto.orderPizza("cheese");
	}
	public static void ch7main3() {
		Ch7SimpleRemoteControl remot = new Ch7SimpleRemoteControl();
		Ch7Light light = new Ch7Light();
		Ch7LightOnCommand ligon = new Ch7LightOnCommand(light);
		remot.setCommand(ligon);
		remot.buttonWasPressed();
		Ch7LightOffCommand ligoff = new Ch7LightOffCommand(light);
		remot.setCommand(ligoff);
		remot.buttonWasPressed();
		Ch7GarageDoor door = new Ch7GarageDoor();
		Ch7GarageDoorOpenCommand doorop = new Ch7GarageDoorOpenCommand(door);
		remot.setCommand(doorop);
		remot.buttonWasPressed();
		Ch7GarageDoorDownCommand doordown = new Ch7GarageDoorDownCommand(door);
		remot.setCommand(doordown);
		remot.buttonWasPressed();	
	}
	public static void ch7main2() {
		System.out.println("before Singleton");
		Ch7SingPatternLoggerBefore bef = new Ch7SingPatternLoggerBefore();
		System.out.println(bef.readEntireLog());
		Ch7SingPatternLoggerBefore bef2 = new Ch7SingPatternLoggerBefore();
		System.out.println(bef2.readEntireLog());

		System.out.println("after Singleton");
		Ch7SingPatternLogger aft = Ch7SingPatternLogger.getInstance();
		System.out.println(aft.readEntireLog());
		Ch7SingPatternLogger aft2 = Ch7SingPatternLogger.getInstance();
		System.out.println(aft2.readEntireLog());
	}
	public static void ch7main1() {
		Ch7WildTurkey tur = new Ch7WildTurkey();
		Ch7MallardDuck duck = new Ch7MallardDuck();
		Ch7TurkeyAdapter pdap = new Ch7TurkeyAdapter(tur);
		System.out.println("Duck");
		duck.quack();
		duck.fly();
		System.out.println("\n");
		System.out.println("Turkey");
		tur.gobble();
		tur.fly();
		System.out.println("\n");
		System.out.println("After adapter");
		pdap.quack();
		pdap.fly();
	}	
	public static void ch5main3() {
		Ch5ArrayLinkedList list = new Ch5ArrayLinkedList();
		list.printArrayList();
		list.printLinkedList();
	}
	public static void ch5main2() {
	Ch5ObsPatternView view = new Ch5ObsPatternView();
	Ch5ObsPatternModel model = new Ch5ObsPatternModel();
	model.addObserver(view);
	model.changeSomething();
	}
	public static void ch5main1() {
	 	Ch5FixedPointv1New imm = new Ch5FixedPointv1New(3,5);
		System.out.println("version1 getX :" +imm.getX() + "getY:" +imm.getY());
		//Ch5FixedPointv1NewSub imm2 = new Ch5FixedPointv1MewSub(3,5);	
	//System.out.println("version1 setX:" +imm2.setX(10) + "setY:" +imm2.setY(20));
		Ch5FixedPointv2Inheritance ver2 = new Ch5FixedPointv2Inheritance(10,20);
		System.out.println("version2 getx:" +ver2.getX()+"gety:"+ver2.getY());
		Ch5FixedPointv3Association ver3 = new Ch5FixedPointv3Association(2,3);
		System.out.println("version3 getx:" +ver3.getX() +"getY:"+ver3.getY()+"getLocation:"+ver3.getLocation());
	}
	public static void ch4main2() {
		ch4getRoomCharge chr = new ch4getRoomCharge();
		ch4Refactoring fac = new ch4Refactoring();
		ch4Refactoring2 fac2 = new ch4Refactoring2();
		ch4Refactoring3 fac3 = new ch4Refactoring3();

	 	System.out.println("getRoomCharge:" +chr.getRoomCharge());
		System.out.println("Refactoring1:"+fac.getTotalBill());
		System.out.println("Refactoring2:"+fac2.getTotalBill());
		System.out.println("Refactoring3:"+fac3.getTotalBill());
	}
  public static void ch4main1version3(){
         ch3Triangle2 t = new ch3Triangle2(new Point(0,0), new Point(1,1), new                    Point(2,2));
         ch3ColoredTriangle3 rct = new ch3ColoredTriangle3(Color.red,new Point  (0,0), new Point(1,1), new Point(2,2));
                ch3ColoredTriangle3 bct = new ch3ColoredTriangle3(Color.blue,                   new Point(0,0), new Point(1,1), new Point(2,2));
        System.out.println("reflexive:"+t.equals(t)+"  " +rct.equals(rct));
        System.out.println("Symmetric:"+t.equals(rct)+"  " +rct.equals(t));
        System.out.println("transitive:"+t.equals(rct)+"  "+rct.equals(bct)+"  "+t.equals(bct));
        for(int i=0; i<3; i++){
        System.out.println("consistnet:"+t.equals(rct)+" "+rct.equals(t));
        }
        System.out.println("non-null:"+t.equals(null)+ " "+rct.equals(null));
}


	public static void ch4main1version2(){
         ch3Triangle t = new ch3Triangle(new Point(0,0), new Point(1,1), new 			Point(2,2));
         ch3ColoredTriangle2 rct = new ch3ColoredTriangle2(Color.red,new Point	(0,0), new Point(1,1), new Point(2,2));
                ch3ColoredTriangle2 bct = new ch3ColoredTriangle2(Color.blue, 			new Point(0,0), new Point(1,1), new Point(2,2));
        System.out.println("reflexive:"+t.equals(t)+"  " +rct.equals(rct));
        System.out.println("Symmetric:"+t.equals(rct)+"  " +rct.equals(t));
        System.out.println("transitive:"+t.equals(rct)+"  "+rct.equals(bct)+"  "+t.equals(bct));
        for(int i=0; i<3; i++){
        System.out.println("consistnet:"+t.equals(rct)+" "+rct.equals(t));
	}
        System.out.println("non-null:"+t.equals(null)+ " "+rct.equals(null));
}

	public static void ch4main1version1(){
		ch3Triangle t = new ch3Triangle(new Point(0,0), new Point(1,1), new Point(2,2));
		ch3ColoredTriangle ct = new ch3ColoredTriangle(Color.red,new Point(0,0), new Point(1,1), new Point(2,2));
		ch3ColoredTriangle ct2 = new ch3ColoredTriangle(Color.blue, new Point(0,0), new Point(1,1), new Point(2,2));
	System.out.println("reflexive:"+t.equals(t)+"  " +ct.equals(ct));
	System.out.println("Symmetric:"+t.equals(ct)+"  " +ct.equals(t));
	System.out.println("transitive:"+t.equals(ct)+"  "+ct.equals(ct2)+"  "+t.equals(ct2));
	for(int i=0; i<3; i++) 
	System.out.println("consistnet:"+t.equals(ct)+" "+ct.equals(t));
	System.out.println("non-null:"+t.equals(null)+ " "+ct.equals(null));
}						 
							
	public static void ch3main3() {
		String[] str = {"one", "two", "three", "four"};
		ch3StringComparator strcom = new ch3StringComparator();
		ch3Sorter strsort = new ch3Sorter();
		for(int i=0; i<str.length; i++)
		System.out.println("string Sort 전: "+ str[i]); 
		strsort.sort(str, strcom);

		Integer[] in = {20, 40, 12, 54};
		for(int i=0; i<in.length; i++)
		System.out.println("int Sort 전: " + in[i]);
		ch3Comparator intcom = new ch3IntegerComparator();
		ch3Sorter intsort = new ch3Sorter();
		intsort.sort(in, intcom);
			} 
			

	public static void ch3main2() {
		ch3Person me = new ch3Person("kimyujin", "korea,incheon");
		ch3Employee emp = new ch3Employee(me, 150); 
		System.out.println("Name="+ emp.getName() + " address=" + emp.getAddress() + "  salary=" + emp.getSalary());
		ch3Student std = new ch3Student(me, 4.5);
		System.out.println("Name=" +std.getName() + "  address=" + std.getAddress() + "  grade=" + std.getGrade());
}

	public static void ch3main1() {
		ch3MutableRectangle rec =new ch3MutableRectangle(5,5,10,20);
		System.out.println("Rectangle- width:" + rec.getWidth() +"Height:" +rec.getHeight() );
		 ch3MutableSquare sqr = new ch3MutableSquare(5,5,10,10);
                System.out.println("Square- width:" + sqr.getWidth() +"Height:" +sqr.getHeight());
      		System.out.println("Rectangle width & Height change");
		rec.setSize(60,40);
		System.out.println("Rectangle- width:"+ rec.getWidth() +"Height:"+rec.getHeight());

        	System.out.println("Square Width & Height change");
		sqr.setSize(30);
		System.out.println("Square- width:"+ sqr.getWidth() +"Height:"+sqr.getHeight());
}
	public static void ch2main1()
	{
  	   EnhancedRectangle Rec = new EnhancedRectangle(3,3,10,20);
	   Point p1 = Rec.getCenter();
           System.out.println("center x:" +p1.getX() +"center y:"+p1.getY());
           Rec.setCenter(50, 50);
           Point p2 = Rec.getCenter();
	   System.out.println("center x:" +p2.getX() +"center y:"+p2.getY());
           }

	 public static void ch2main2()
        {
 	   int totalCapacity1 = 0;
	    int totalCapacity2 = 0;

	   Automobile[] fleet = new Automobile[3];
	   fleet[0] = new Sedan(Color.black);
	   fleet[1] = new Minivan(Color.blue);
	   fleet[2] = new SportsCar(Color.red);
	  for ( int i = 0; i< fleet.length; i++)
	  {
		if(fleet[i] instanceof Sedan)
			totalCapacity1 += ((Sedan) fleet[i]).getCapacity();
		else if(fleet[i] instanceof Minivan)
               		 totalCapacity1 += ((Minivan) fleet[i]).getCapacity();
		else if(fleet[i] instanceof SportsCar)
               		 totalCapacity1 += ((SportsCar) fleet[i]).getCapacity();
		else
			totalCapacity1 += fleet[i].getCapacity();
	   }
	  
 	  for(int i=0; i<fleet.length; i++)
	   totalCapacity2 +=fleet[i].getCapacity();

	System.out.println("(instanceof)totalCapacity1:"+totalCapacity1);
	System.out.println("totalCapacity2:" +totalCapacity2);
        }
	
 	public static void ch2main3()
        {
		ArrayList<String> list = new ArrayList<String>();
		list.add("First");
		list.add("Second");
		list.add("Third");
		list.add("Fourth");
 		
		Iterator<String> itr = list.iterator();

		System.out.println("==========ArrayList================");
		System.out.println("next사용하여 출력\n");
		while(itr.hasNext())
		{
			String curStr = itr.next();
			System.out.println(curStr);
		}
		
		System.out.println("size사용하여 출력\n");
		for(int i=0; i<list.size(); i++)
			System.out.println(list.get(i));

		System.out.println(list.size());
             
		list.clear();
		if(list.isEmpty())
		{
			System.out.println("ArrayList clear OK");
		}
		
		System.out.println("============LinkedList=========="); 
		LinkedList<String> list2 = new LinkedList<String>();
                list2.add("First");
                list2.add("Second");
                list2.add("Third");
                list2.add("Fourth");

                Iterator<String> itr2 = list2.iterator();

                System.out.println("next사용하여 출력\n");
                while(itr2.hasNext())
                {
                        String curStr = itr2.next();
                        System.out.println(curStr);
                }

                System.out.println("size사용하여 출력\n");
                for(int i=0; i<list2.size(); i++)
                        System.out.println(list2.get(i));

                System.out.println(list2.size());
		
		 list2.clear();
                if(list2.isEmpty())
                {
                        System.out.println("LinkedList clear OK");
                }

	
        }
}


