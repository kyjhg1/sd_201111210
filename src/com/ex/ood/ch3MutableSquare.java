package com.ex.ood;

import com.ex.ood.ch3Rectangle;

public class ch3MutableSquare extends ch3Rectangle {

	public ch3MutableSquare(int x, int y, int w, int h) {
		super(x, y, w, h);
	}

	public void setSize(int s) {
		w = s;
		h = s;
	}
}
