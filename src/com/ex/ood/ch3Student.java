package com.ex.ood;

import com.ex.ood.ch3Person;

public class ch3Student {

	private ch3Person me;
	private double grade;

	public ch3Student(ch3Person p, double g) {
		me = p;
		grade = g;
	}

	public String getName() {
		return me.getName();
	}

	public String getAddress() {
		return me.getAddress();
	}

	public double getGrade() {
		return grade;
	}
} 
