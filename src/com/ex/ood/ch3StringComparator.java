package com.ex.ood;

import com.ex.ood.ch3Comparator;

public class ch3StringComparator implements ch3Comparator{ //implements는 인터페이스 상속할때....

	public int compare(Object o1, Object o2) {
		String s1 = (String)o1;
		String s2 = (String)o2;	
		return s1.compareTo(s2);
	}
}

