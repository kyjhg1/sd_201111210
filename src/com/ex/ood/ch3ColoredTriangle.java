package com.ex.ood;

import java.awt.*;
import java.lang.*;

public class ch3ColoredTriangle extends ch3Triangle{
	private Color color;
	public ch3ColoredTriangle(Color c, Point p1, Point p2, Point p3) {			
		super(p1, p2, p3);
		if(c==null) 
		c=Color.red;
		color=c;
	}
	public boolean equals(Object obj){
		if(!(obj instanceof ch3ColoredTriangle))
		return false;
		
		ch3ColoredTriangle otherTriangle = (ch3ColoredTriangle)obj;
		return super.equals(otherTriangle)&&this.color.equals(otherTriangle.color);
}
}	
